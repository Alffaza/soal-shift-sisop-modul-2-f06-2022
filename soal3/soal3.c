#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>

void writeTxt(){
    FILE * fPtr;
    
    fPtr = fopen("/home/afif/modul2/air/list.txt", "w");

    if(fPtr == NULL)
    {
        /* File not created hence exit */
        printf("Unable to create file.\n");
        exit(EXIT_FAILURE);
    }
    
    DIR *dp;
    struct dirent *ep;
    char pathAwal[]="/home/afif/modul2/air/";
    char pathSrc[]="/home/afif/modul2/air/";
    char hasilAwal[]="";
    char hasil[]="";
    int r;
     struct stat info;
    
    dp = opendir(pathAwal);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
      
          	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
          	strcpy(pathSrc,"/home/afif/modul2/air/");
          	strcat(pathSrc,ep->d_name);
          	
          	strcpy(hasil,hasilAwal);
          	r = stat(pathSrc, &info);
    		if( r==-1 )
    		{		
      			fprintf(stderr,"File error\n");
        		exit(1);
    		}

    		struct passwd *pw = getpwuid(info.st_uid);

    		if (pw != 0) strcat(hasil,pw->pw_name);
    		strcat(hasil,"_");
    		if( info.st_mode & S_IRUSR )
        		strcat(hasil,"r");
    		if( info.st_mode & S_IWUSR )
        		strcat(hasil,"w");
    		if( info.st_mode & S_IXUSR )
        		strcat(hasil,"x");
        		
        	strcat(hasil,"_");
        	strcat(hasil,ep->d_name);
        	//puts(hasil);
        	//writeTxt(hasil);
        	fprintf(fPtr,"%s\n", hasil);
        }

      	(void) closedir (dp);
    } else perror ("Couldn't open the directory");
    
    fclose(fPtr);
}

void removeBird2(char *path){
	//puts(path);
	
	pid_t child_id;
	child_id = fork();
	int status;
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"rm",path, NULL};
        	execv("/bin/rm", argv);
	}
	else{
		while((wait(&status)) > 0);
		writeTxt();
	}
        
        return;
}

void removeBird(){
	DIR *dp;
    	struct dirent *ep;
    	char pathAwal[]="/home/afif/modul2/darat/";
    	char pathSrc[]="/home/afif/modul2/darat/";

	pid_t child_id;
	child_id = fork();
	int status;
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
    	dp = opendir(pathAwal);

    	if (dp != NULL)
    	{
     	while ((ep = readdir (dp))) {
      	
        	  	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
        	  	strcpy(pathSrc,pathAwal);
        	  	strcat(pathSrc,ep->d_name);
          		if(strstr(pathSrc,"bird")) {
          			if(fork()==0) continue;
          			removeBird2(pathSrc);
			}
        	  		
        }
        (void) closedir (dp);
        } else perror ("Couldn't open the directory");
    	}
	
}

void removeAnimal(){
	pid_t child_id;
	child_id = fork();
	int status;
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"rm","-d", "animal", NULL};
    		execv("/bin/rm", argv);		
	}
	else{
		while((wait(&status)) > 0);
		removeBird();
	}
	
	return;
}

void moveDarat(){
    DIR *dp;
    struct dirent *ep;
    char pathAwal[]="/home/afif/animal/";
    char pathSrc[]="/home/afif/animal/";
    int status;
    
    dp = opendir(pathAwal);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
      
          	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
          	if(fork()==0) continue;
          	strcpy(pathSrc,pathAwal);
          	strcat(pathSrc,ep->d_name);
          	
          	char *argv[] = {"mv",pathSrc, "/home/afif/modul2/darat", NULL};
          	execv("/bin/mv", argv);	
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    
    
    
    return;
}

void moveAir(){
    DIR *dp;
    struct dirent *ep;
    char pathAwal[]="/home/afif/animal/";
    char pathSrc[]="/home/afif/animal/";
    int status;
    
    pid_t child_id;
    
    child_id = fork();
    if (child_id < 0){
    	exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
    	dp = opendir(pathAwal);

    	if (dp != NULL)
    	{
     	while ((ep = readdir (dp))) {
      	
        	  	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
        	  	if(fork()==0) continue;
        	  	strcpy(pathSrc,pathAwal);
        	  	strcat(pathSrc,ep->d_name);
          	
        	  	char *argv[] = {"mv",pathSrc, "/home/afif/modul2/air", NULL};
        	  	execv("/bin/mv", argv);	
        }
        (void) closedir (dp);
        } else perror ("Couldn't open the directory");
    }
    else{
    	while((wait(&status)) > 0);
    	removeAnimal();
    }
    
    return;
}

void unzipDarat(){
	pid_t child_id;
	int status;
	
	child_id = fork();
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"unzip", "animal.zip","*darat*", NULL};
		execv("/bin/unzip", argv);		
	}
	else{
		while((wait(&status)) > 0);
		moveDarat();
	}
	return;
}

void unzipAir(){
	pid_t child_id;
	int status;
	
	child_id = fork();
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"unzip", "animal.zip","*air*", NULL};
		execv("/bin/unzip", argv);		
	}
	else{
		while((wait(&status)) > 0);
		moveAir();
		
	}
	return;
}

void makeDir(){
	pid_t child_id;
	pid_t child_id2;
	int status;
	
	child_id = fork();
	child_id2 = fork();
	
	if (child_id < 0 || child_id2 < 0){
		exit(EXIT_FAILURE);
	}
	// membuat folder
	if (child_id == 0){
		char *argv[] = {"mkdir", "-p", "modul2/darat", NULL};
		execv("/bin/mkdir", argv);		
	}
	sleep(3);
	if (child_id2 == 0){
		char *argv[] = {"mkdir", "-p", "modul2/air", NULL};
		execv("/bin/mkdir", argv);
	}
	else{
		while((wait(&status)) > 0);
		unzipDarat();
		unzipAir();
	}
	
	return;
}

int main()
{
	makeDir();
	
	return 0;
}
