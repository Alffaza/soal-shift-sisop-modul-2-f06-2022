#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

char input[10][10][100], kategori[10][50];
int jenis = 0, last[10];

void createFolder(char *path)
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        char *cmd[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}

void extractFile()
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        char *cmd[] = {"unzip", "-q", "drakor.zip", "*.png", "-d", "shift2/drakor", NULL};
        execv("/bin/unzip", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}

void delBack(char *str, char *sub_str)
{
    size_t len = strlen(sub_str);
    char *s = str;
    while ((s = strstr(s, sub_str)) != NULL)
        memmove(s, s + len, strlen(s + len) + 1);
}

void copyFile(char *src, char *path)
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        char *cmd[] = {"cp", src, path, NULL};
        execv("/bin/cp", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}

void renameFile(char *src, char *path)
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        char *cmd[] = {"mv", src, path, NULL};
        execv("/bin/mv", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}

void categorize()
{
    DIR *folder;
    struct dirent *de;
    char path[] = "shift2/drakor/";

    folder = opendir(path);
    while ((de = readdir(folder)) != NULL)
    {
        if (strcmp(de->d_name, ".") && strcmp(de->d_name, ".."))
        {
            char edit[100];
            strcpy(edit, de->d_name);
            delBack(edit, ".png");

            char replace = '_';
            char new = ';';

            int len = strlen(edit);
            if (strstr(edit, "_"))
            {
                for (int i = 0; i < len; i++)
                {
                    if (edit[i] == replace)
                    {
                        edit[i] = new;
                        break;
                    }
                }
            }

            char filmName[50], year[5], category[20];
            int ind = 1;
            char *tok = strtok(edit, ";");
            while (tok != NULL)
            {
                char src[100], dest[100], from[100];
                strcpy(src, path);
                strcpy(dest, path);
                strcpy(from, path);

                if (ind % 3 == 0)
                {
            
                    strcpy(category, tok);
                    strcat(src, de->d_name);
                    strcat(dest, category);

                    DIR *myfolder = opendir(dest);
                    if (myfolder)
                        closedir(myfolder);
                    else if (ENOENT == errno)
                    {
                        createFolder(dest);
                        strcpy(kategori[jenis++], category);
                    }

                    copyFile(src, dest);
                    strcat(from, category);
                    strcat(from, "/");
                    strcat(from, de->d_name);

                    strcat(dest, "/");
                    strcat(dest, filmName);
                    strcat(dest, ".png");

                    renameFile(from, dest);
                    for (int i = 0; i < jenis; i++)
                    {
                        if (strcmp(kategori[i], category) == 0)
                        {
                            char tmp[50];
                            strcpy(tmp, year);
                            strcat(tmp, "_");
                            strcat(tmp, filmName); 
                            strcpy(input[i][last[i]++],tmp);
                        }
                    }
                }
                else if (ind % 3 == 1)
                    strcpy(filmName, tok);
                else if (ind % 3 == 2)
                    strcpy(year, tok);
                tok = strtok(NULL, ";");
                ind++;
            }
        }
    }
    closedir(folder);
}

void sort()
{
    char tmp[100];

    for(int i=0;i<jenis;i++)
    {
        for(int j=0;j<last[i];j++)
        {
            for(int k=0;k<last[i]-j-1;k++)
            {
                if(strcmp(input[i][k], input[i][k+1]) > 0){
                    strcpy(tmp, input[i][k]);
                    strcpy(input[i][k], input[i][k+1]);
                    strcpy(input[i][k+1], tmp);
                }
            }
        }
    }
}

void inputFile()
{
    char path[] = "shift2/drakor/";
    for (int i = 0; i < jenis; i++)
    {
        char dest[100];
        strcpy(dest, path);
        strcat(dest, kategori[i]);
        for(int j=0;j<last[i];j++)
        {
            char edit[100];
            strcpy(edit, input[i][j]);
            char filmName[50], year[5];
            int ind = 1;
            char *tok = strtok(edit, "_");
            while (tok != NULL)
            {
                if (ind % 2 == 0)
                    strcpy(filmName, tok);
                else if (ind % 2 == 1)
                    strcpy(year, tok);
                tok = strtok(NULL, ";");
                ind++;
            }
            char loc[150];
            strcpy(loc, dest);
            strcat(loc, "/data.txt");
            if (access(loc, F_OK    ) == 0)
            {
                FILE *wfile = fopen(loc, "a");
                fprintf(wfile, "\nnama : %s\nrilis : tahun %s\n", filmName, year);
                fclose(wfile);
            }
            else
            {
                FILE *wfile = fopen(loc, "w");
                fprintf(wfile, "kategori : %s\n\nnama : %s\nrilis : tahun %s\n", kategori[i], filmName, year);
                fclose(wfile);
            }
        }
    }
}

int main()
{
    memset(last, 0, sizeof(last));
    createFolder("shift2/drakor");
    extractFile();
    categorize();
    sort();
    inputFile();
}
