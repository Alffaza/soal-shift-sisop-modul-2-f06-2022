# soal-shift-sisop-modul-2-F06-2022

Ridzki Raihan Alfaza 5025201178  
Muhammad Afif Dwi Ardhiansyah 5025201212  
Antonio Taifan Montana 5025201219

---

# SOAL 1

```c
#include<json-c/json.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <dirent.h>
#include <time.h>
```

library yang digunakan untuk nomor 1

```c
#define STARTEATING if(fork()==0){
#define FORK }if(fork()==0){
#define SPOON }else{while ((wait(&status)) > 0);
#define FINISHEATING }
```

menggunakan define untuk mempermudah pembuatan child process

```c
typedef struct {
    char name[50];
    char rarity[3];
}gachaItem;
```

struct gacha item yang berisi nama item dan rarity, rarity tersimpan sebagai string karena json yang didownload juga menyimpan rarity sebagai string

```c
    pid_t pid, sid;        // Variabel untuk menyimpan PID
    int status;
```

deklarasi variabel pid untuk pembuatan daemon process dan status untuk function wait

```c
    time_t startTime_t, currTime_t;
    struct tm *startTime;
    startTime = (struct tm*)malloc(sizeof(struct tm));
```

pembuatan variabel untuk menyimpan waktu, `struct tm` berisi struct yang memiliki attribut tahun, bulan, hari, dst.

```c
    //set time 30 mar 2022 4:44
    startTime->tm_year = 2022 - 1900;
    startTime->tm_mon = 3 - 1;
    startTime->tm_mday = 30;
    startTime->tm_hour = 4;
    startTime->tm_min = 44;
    startTime->tm_sec = 0;
    startTime->tm_isdst = -1;
```

set `startTime` menjadi 30 mar 2022 4:44 sesuai dengan ketentuan soal (year dan month dikurangi dengan 1900 dan 1 karena default dari `struct tm` adalah pada 1900 januari)

```c
    startTime_t = mktime(startTime);
```

mengubah startTime menjadi class `time_t`

```c
pid = fork(); // Menyimpan PID dari Child Process

        /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
```

pembuatan daemon process

```c
    currTime_t = time(NULL);
    while (currTime_t < startTime_t){
        sleep(1);
        currTime_t = time(NULL);
    }
```

set `currTime_t` menjadi system time lalu menunggu sampai sudah lewat dengan waktu `startTime_t`

```c
    STARTEATING
        if (fopen("characters.zip", "r"))
        return 0;
        printf("gaada character\n");
        char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "characters.zip", NULL};
        execv("/bin/wget", argv);
    SPOON
        printf("ada character\n");
    FORK
        if(fopen("weapons.zip", "r"))return 0;
        printf("gaada weapon\n");
        char *argv[] = {"wget", "-q","https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "weapons.zip", NULL};
        execv("/bin/wget", argv);
    SPOON
        printf("ada weapon\n");
```

download zip dari google drive menggunakan `execv`, printf digunakan untuk debug saat pembuatan

```c
    FORK
        if(opendir("characters")) return 0;
        char *argv[] = {"unzip","-q" ,"characters.zip", NULL};
        execv("/bin/unzip", argv);
    SPOON
    FORK
        if(!fopen("characters.zip", "r"))return 0;
        char *argv[] = {"rm", "characters.zip", NULL};
        execv("/bin/rm", argv);
    SPOON
    FORK
        if(opendir("weapons")) return 0;
        char *argv[] = {"unzip", "-q","weapons.zip", NULL};
        execv("/bin/unzip", argv);
    SPOON
    FORK
        if(!fopen("weapons.zip", "r"))return 0;
        char *argv[] = {"rm", "weapons.zip", NULL};
        execv("/bin/rm", argv);
    SPOON
```

unzip file yang sudah didownload lalu delete file zip setelah selesai process unzip

```c
    FORK
        if(opendir("gacha_gacha"))return 0;
        char *argv[] = {"mkdir", "gacha_gacha", NULL};
        execv("/bin/mkdir", argv);
    SPOON
    FINISHEATING
```

membuat folder "gacha_gacha"

```c
    int wpnCnt = 0, chrCnt = 0;
    DIR  *currDir;
    struct dirent * entry;
```

deklarasi variabel sebelum parsing json

```c
    currDir = opendir("weapons");
    while ((entry = readdir(currDir)) != NULL)
    {
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        wpnCnt++;
    }
    closedir(currDir);
```

membuka isi dari folder "weapons" dan menghitung semua file di dalamnya

```c
    currDir = opendir("characters");
    while ((entry = readdir(currDir)) != NULL)
    {
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        chrCnt++;
    }
    closedir(currDir);
```

membuka isi dari folder "characters" dan menghitung semua file di dalamnya

```c
    gachaItem *weapons, *characters;
    weapons = (gachaItem *)malloc(wpnCnt*sizeof(gachaItem));
    characters = (gachaItem *)malloc(chrCnt*sizeof(gachaItem));
```

allokasi memori array `gachaItem` berisi weapons dan characters

```c
    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;
    char *buffer;
    buffer = (char *)malloc(4096*sizeof(char));
    int cnt = 0;
```

deklarasi variabel untuk parsing json.

```c
    currDir = opendir("weapons");
    while ((entry = readdir(currDir)) != NULL)
    {
        char filename[50] = "weapons/";
        strcat(filename, entry->d_name);
        FILE *f = fopen(filename, "r");
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        fread(buffer, sizeof(char)*4096, 1, f);
        fclose(f);
        parsed_json = json_tokener_parse(buffer);
        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);
        //printf("%s %s\n", json_object_get_string(name), json_object_get_string(rarity));
        gachaItem *newItem = malloc(sizeof(gachaItem));
        strcpy(newItem->name, json_object_get_string(name));
        strcpy(newItem->rarity, json_object_get_string(rarity));
        weapons[cnt++] = *newItem;
    }
    closedir(currDir);
```

iterasi semua file dan parsing json di folder weapons lalu memasuki semuanya ke dalam array `weapons`

```c
    cnt = 0;
    currDir = opendir("characters");
    while ((entry = readdir(currDir)) != NULL)
    {
        char filename[50] = "characters/";
        strcat(filename, entry->d_name);
        FILE *f = fopen(filename, "r");
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        fread(buffer, sizeof(char)*4096, 1, f);
        fclose(f);
        parsed_json = json_tokener_parse(buffer);
        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);
        //printf("%s %s\n", json_object_get_string(name), json_object_get_string(rarity));
        gachaItem *newItem = malloc(sizeof(gachaItem));
        strcpy(newItem->name, json_object_get_string(name));
        strcpy(newItem->rarity, json_object_get_string(rarity));
        characters[cnt++] = *newItem;
    }
    closedir(currDir);
```

iterasi semua file dan parsing json di folder weapons lalu memasuki semuanya ke dalam array `characters`

```c
    chdir("gacha_gacha");

    int gachaTries = 0;
    int primogems = 79000;
    int primoCost = 160;

    srand(time(NULL));
    int randomNum;
    char folderName[100];
```

deklarasi variabel sebelum melakukan gacha

```c
    while(1){
        if(primogems <= primoCost)
            break;
        time_t now = time(NULL);
        struct tm *tm_struct = localtime(&now);
        char filename[100];
        char filePath[100];
        char triesStr[10];
        sprintf(triesStr, "%d", gachaTries);

        strftime(filename, 20, "%H:%M:%S", tm_struct);
        strcat(filename,"_gacha_");
        strcat(filename, triesStr);
        strcat(filename, ".txt");

        if(gachaTries % 90 == 0){
            strcpy(folderName, "total_gacha_");
            strcat(folderName, triesStr);
            STARTEATING
                if(opendir(folderName))return 0;
                char *argv[] = {"mkdir", folderName, NULL};
                execv("/bin/mkdir", argv);
            SPOON
            FINISHEATING
        }
        strcpy(filePath, folderName);
        strcat(filePath, "/");
        strcat(filePath, filename);


        STARTEATING
            char *argv[] = {"touch", filePath, NULL};
            execv("/bin/touch", argv);
        SPOON
            FILE *f = fopen(filePath, "w");
            char fileContent[400]="";
            for (int i = 0; i < 10; i++)
            {
                primogems -= primoCost;
                gachaItem gachaHaha;
                if (++gachaTries % 2)
                {                                   //ganjil = character
                    randomNum = rand() % chrCnt;
                    gachaHaha = characters[randomNum];
                }
                else
                {                                   //genap = weapon
                    randomNum = rand() % wpnCnt;
                    gachaHaha = weapons[randomNum];
                }
                char primoStr[6];
                sprintf(triesStr, "%d", gachaTries);
                sprintf(primoStr, "%d", primogems);
                strcat(fileContent, triesStr);
                strcat(fileContent, "_");
                strcat(fileContent, (gachaTries%2 ? "characters":"weapons"));
                strcat(fileContent, "_");
                strcat(fileContent, gachaHaha.rarity);
                strcat(fileContent, "_");
                strcat(fileContent, gachaHaha.name);
                strcat(fileContent, "_");
                strcat(fileContent, primoStr);
                strcat(fileContent, "\n");

                if (primogems <= primoCost)
                    break;
            }
            strcat(fileContent, "\0");
            printf("%s", fileContent);
            fwrite(fileContent, sizeof(char), strlen(fileContent), f);
            fclose(f);
            FINISHEATING

            sleep(1);
    }
```

melakukan gacha

```c
        if(primogems <= primoCost)
            break;
```

jika gabisa gacha lagi stop loop

```c
        struct tm *tm_struct = localtime(&now);
        char filename[100];
        char filePath[100];
        char triesStr[10];
        sprintf(triesStr, "%d", gachaTries);

        strftime(filename, 20, "%H:%M:%S", tm_struct);
        strcat(filename,"_gacha_");
        strcat(filename, triesStr);
        strcat(filename, ".txt");
```

membuat nama file gacha

```c
        if(gachaTries % 90 == 0){
            strcpy(folderName, "total_gacha_");
            strcat(folderName, triesStr);
            STARTEATING
                if(opendir(folderName))return 0;
                char *argv[] = {"mkdir", folderName, NULL};
                execv("/bin/mkdir", argv);
            SPOON
            FINISHEATING
        }
```

jika try mod 90 maka membuat folder baru

```c
        strcpy(filePath, folderName);
        strcat(filePath, "/");
        strcat(filePath, filename);
```

path file gacha

```c
        STARTEATING
            char *argv[] = {"touch", filePath, NULL};
            execv("/bin/touch", argv);
```

membuat file gacha

```c
        SPOON
            FILE *f = fopen(filePath, "w");
            char fileContent[400]="";
            for (int i = 0; i < 10; i++)
            {
                primogems -= primoCost;
                gachaItem gachaHaha;
                if (++gachaTries % 2)
                {                                   //ganjil = character
                    randomNum = rand() % chrCnt;
                    gachaHaha = characters[randomNum];
                }
                else
                {                                   //genap = weapon
                    randomNum = rand() % wpnCnt;
                    gachaHaha = weapons[randomNum];
                }
                char primoStr[6];
                sprintf(triesStr, "%d", gachaTries);
                sprintf(primoStr, "%d", primogems);
                strcat(fileContent, triesStr);
                strcat(fileContent, "_");
                strcat(fileContent, (gachaTries%2 ? "characters":"weapons"));
                strcat(fileContent, "_");
                strcat(fileContent, gachaHaha.rarity);
                strcat(fileContent, "_");
                strcat(fileContent, gachaHaha.name);
                strcat(fileContent, "_");
                strcat(fileContent, primoStr);
                strcat(fileContent, "\n");

                if (primogems <= primoCost)
                    break;
            }
```

pengambilan gacha 10 kali, jika di tengah-tengah loop gabisa gacha lagi, keluar dari loop

```c
            strcat(fileContent, "\0");
            printf("%s", fileContent);
            fwrite(fileContent, sizeof(char), strlen(fileContent), f);
            fclose(f);
            FINISHEATING

            sleep(1);
    }
```

write hasil gacha ke dalam file lalu menunggu 1 detik

```c
    //nunggu sampe 7:44, 30 Maret
    currTime_t = time(NULL);
    int delaySeconds = 3600 * 3;
    while (currTime_t < startTime_t + delaySeconds)
    {
        sleep(1);
        currTime_t = time(NULL);
    }
```

menunggu 3 jam sebelum zip

```c
    chdir("..");
    STARTEATING
        char *argv[] = {"zip", "--password", "satuduatiga", "-r","not_safe_for_wibu.zip", "gacha_gacha", NULL};
        execv("/bin/zip", argv);
    SPOON
    FORK
        char *argv[] = {"rm", "-r", "gacha_gacha" ,NULL};
        execv("/bin/rm", argv);
    SPOON
    FORK
        char *argv[] = {"rm", "-r", "characters" ,NULL};
        execv("/bin/rm", argv);
    SPOON
    FORK
        char *argv[] = {"rm", "-r", "weapons" ,NULL};
        execv("/bin/rm", argv);
    FINISHEATING
```

zip hasil gacha, lalu delete semua folder gacha lain

## Screenshot

<img src=soal1/1.1.png>
<img src=soal1/1.2.png>
<img src=soal1/1.3.png>
<img src=soal1/1.4.png>
<img src=soal1/1.5.png>

## Kendala

- Sempat ada kendala dalam parsing json gagal, ternyata ukuran buffer kurang besar.
- Kendala download menggunakan wget, tidak mendownload folder zip yang benar, kendala tersebut dibenarkan menggunakan argume quiet / `-q`
- Sempat ada kendala execv tidak menjalankan command dengan lancar, dibenarkan dengan cara menambahkan NULL pada argumen terakhir

----
# SOAL 2

Pada soal 2, Japrun mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi di dalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun meminta bantuan untuk menyelesaikan pekerjaannya.
## Membuat Directory Drakor
Diminta untuk membuat directory “/home/[USER]/modul2/shift2/drakor”. Untuk itu dilakukan penulisan kode fungsi pembuatan folder sebagai berikut,
```c
void createFolder(char *path)
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        char *cmd[] = {"mkdir", "-p", path, NULL};
        execv("/bin/mkdir", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}
```
Dengan fungsi pada main dipanggil sebagai berikut,

```c
    createFolder("shift2/drakor");
```
## Extract Zip drakor.zip
Diminta untuk mengekstrak file drakor.zip. Tetapi ekstrak ini harus sesuai kriteria dimana file dengan format .png saja yang terekstrak ke “/home/[USER]/modul2/shift2/drakor/”. Untuk itu dilakukan penulisan kode fungsi mengekstrak file zip sebagai berikut,

```c
    void extractFile()
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        char *cmd[] = {"unzip", "-q", "drakor.zip", "*.png", "-d", "shift2/drakor", NULL};
        execv("/bin/unzip", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}
```
Dengan fungsi pada main dipanggil sebagai berikut,

```c
    extractFile();
```
## Membuat Kategori setiap genre film yang tersedia
Diminta untuk membuat folder dengan nama genre film yang tersedia pada folder “/home/[USER]/modul2/shift2/drakor/.". Untuk itu dilakukan penulisan kode fungsi membuat kategori setiap genre sebagai berikut,

```c
void delBack(char *str, char *sub_str)
{
    size_t len = strlen(sub_str);
    char *s = str;
    while ((s = strstr(s, sub_str)) != NULL)
        memmove(s, s + len, strlen(s + len) + 1);
}
void categorize()
{
    DIR *folder;
    struct dirent *de;
    char path[] = "shift2/drakor/";

    folder = opendir(path);
    while ((de = readdir(folder)) != NULL)
    {
        if (strcmp(de->d_name, ".") && strcmp(de->d_name, ".."))
        {
            char edit[100];
            strcpy(edit, de->d_name);
            delBack(edit, ".png");

            char replace = '_';
            char new = ';';

            int len = strlen(edit);
            if (strstr(edit, "_"))
            {
                for (int i = 0; i < len; i++)
                {
                    if (edit[i] == replace)
                    {
                        edit[i] = new;
                        break;
                    }
                }
            }

            char filmName[50], year[5], category[20];
            int ind = 1;
            char *tok = strtok(edit, ";");
            while (tok != NULL)
            {
                char src[100], dest[100], from[100];
                strcpy(src, path);
                strcpy(dest, path);
                strcpy(from, path);

                if (ind % 3 == 0)
                {
            
                    strcpy(category, tok);
                    strcat(src, de->d_name);
                    strcat(dest, category);

                    DIR *myfolder = opendir(dest);
                    if (myfolder)
                        closedir(myfolder);
                    else if (ENOENT == errno)
                    {
                        createFolder(dest);
                        strcpy(kategori[jenis++], category);
                    }

                    copyFile(src, dest);
                    strcat(from, category);
                    strcat(from, "/");
                    strcat(from, de->d_name);

                    strcat(dest, "/");
                    strcat(dest, filmName);
                    strcat(dest, ".png");

                    renameFile(from, dest);
                    for (int i = 0; i < jenis; i++)
                    {
                        if (strcmp(kategori[i], category) == 0)
                        {
                            char tmp[50];
                            strcpy(tmp, year);
                            strcat(tmp, "_");
                            strcat(tmp, filmName); 
                            strcpy(input[i][last[i]++],tmp);
                        }
                    }
                }
                else if (ind % 3 == 1)
                    strcpy(filmName, tok);
                else if (ind % 3 == 2)
                    strcpy(year, tok);
                tok = strtok(NULL, ";");
                ind++;
            }
        }
    }
    closedir(folder);
}
```
1. Membuat variabel `folder` dan `de`, kemudian membuka folder drakor sesuai path pada variabel `folder` dan membaca isi semua folder drakor dimana setiap file yang ada pada folder tersebut akan diassign di variabel `de` dengan mengabaikan file `.` dan `..`.
2. Menyimpan `de->d_name` atau nama file pada variabel string `edit`.
3. Memanggil function `delBack()` dengan parameter `edit` sebagai string yang ingin dihapus substringnya yang berada di belakang dan `.png` sebagai substring yang ingin dihapus. Cara kerja function ini adalah mengambil panjang substring yang ingin dihapus dan melakukan iterasi untuk menghapus substring pada suatu string dengan function yang ada pada library `string.h` yaitu `memmove` yang memanipulasi string dengan memindahkan indexnya sehingga substring dapat terhapus. Iterasi ini akan terus dilakukan jika masih ditemukan substring pada string tersebut.
4. Karena nama suatu file dapat mengandung 2 film berbeda yang dipisahkan dengan tanda `_`, maka tanda tersebut pada nama file akan diganti menjadi tanda `;` sehingga lebih mudah ketika memisahkan isi-isi pada nama file tersebut nantinya. Penggantian nama file dilakukan dengan iterasi sederhana dimana ketika ditemukan tanda `_`, maka akan digantikan oleh tanda `;`.
5. Membuat variabel string `filmName`, `year`, dan `category`, serta variabel `ind` yang diset valuenya adalah 1. Variabel `filmName` digunakan untuk menyimpan data berupa nama film dari sebuah file, variabel `year` digunakan untuk menyimpan data berupa tahun rilis film dari sebuah file, dan variabel `category` digunakan untuk menyimpan data berupa jenis film dari sebuah file. Sedangkan untuk variabel `ind` sendiri merupakan variabel yang digunakan sebagai penanda saat pemisahan konten file (nama film, tahun rilis film, atau jenis film) yang akan terus bertambah 1 setiap konten file yang ditemukan.
6. Pemisahan konten file dilakukan dengan function yang ada pada library `string.h` yaitu `strtok` dengan separator `;` hingga akhir string. Disinilah variabel-variabel tadi mulai bekerja. 
7. Saat `ind` dimodulo 3 menghasilkan nilai 1, maka konten file tersebut merupakan nama film sehingga konten file akan dimasukkan pada variabel `filmName`.
8. Saat `ind` dimodulo 3 menghasilkan nilai 2, maka konten file tersebut merupakan tahun rilis film sehingga konten file akan dimasukkan pada variabel `year`.
9. Saat `ind` dimodulo 3 menghasilkan nilai 0, maka konten file tersebut merupakan jenis film sehingga konten file akan dimasukkan pada variabel `category`. Kemudian, akan dibuat variabel string `dest` dengan isinya adalah path folder `drakor` dan akan ditambahkan variabel `category` tadi pada variabel `dest` sebagai path lanjutan dari folder `drakor`. Variabel `myFolder` dibuat dimana variabel tersebut akan menampung path `dest`. Jika folder yang sesuai dengan path `dest` tersebut belum ada, maka akan dibuat folder dengan path tersebut. 
10. Lakukan iterasi pada setiap file hingga selesai sehingga setiap film terkategorikan sesuai jenisnya.

Dengan fungsi pada main dipanggil sebagai berikut,

```c
    categorize();
```

## Memindahkan file ke folder dengan kategori yang sesuai dan merename nama file

Terdapat function renameFile() dengan parameter src yang merupakan nama asli poster dan path yang merupakan nama pengganti poster. Pada function ini berjalan instruksi yang dimulai dengan pembuatan child process yang akan menjalankan command execv dengan argumen {"mv", src, path, NULL} yang berarti merename poster dengan manipulasi menggunakan mv yang memindahkan file sesuai dengan path yang telah ditentukan. Untuk itu dilakukan penulisan kode fungsi memindahkan file png dan merename.

```c
    void renameFile(char *src, char *path)
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        char *cmd[] = {"mv", src, path, NULL};
        execv("/bin/mv", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}
```

```c
void copyFile(char *src, char *path)
{
    pid_t child_process;
    child_process = fork();
    int status;

    if (child_process == 0)
    {
        char *cmd[] = {"cp", src, path, NULL};
        execv("/bin/cp", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}
```

```c
                char src[100], dest[100], from[100];
                strcpy(src, path);
                strcpy(dest, path);
                strcpy(from, path);

                if (ind % 3 == 0)
                {
            
                    strcpy(category, tok);
                    strcat(src, de->d_name);
                    strcat(dest, category);

                    DIR *myfolder = opendir(dest);
                    if (myfolder)
                        closedir(myfolder);
                    else if (ENOENT == errno)
                    {
                        createFolder(dest);
                        strcpy(kategori[jenis++], category);
                    }

                    copyFile(src, dest);
                    strcat(from, category);
                    strcat(from, "/");
                    strcat(from, de->d_name);

                    strcat(dest, "/");
                    strcat(dest, filmName);
                    strcat(dest, ".png");

                    renameFile(from, dest);
                }
```
## Membuat file "data.txt" di setiap folder kategori yang berisi nama dan tahun rilis semua drama korea.
```c
    char input[10][10][100], kategori[10][50];
    int jenis = 0, last[10];
```

```c
                    char dest[100];
                strcpy(dest, path);
            
                    strcpy(category, tok);
                    strcat(dest, category);

                    DIR *myfolder = opendir(dest);
                    if (myfolder)
                        closedir(myfolder);
                    else if (ENOENT == errno)
                    {
                        createFolder(dest);
                        strcpy(kategori[jenis++], category);
                    }
                    for (int i = 0; i < jenis; i++)
                    {
                        if (strcmp(kategori[i], category) == 0)
                        {
                            char tmp[50];
                            strcpy(tmp, year);
                            strcat(tmp, "_");
                            strcat(tmp, filmName); 
                            strcpy(input[i][last[i]++],tmp);
                        }
                    }
```

```c
void sort()
{
    char tmp[100];

    for(int i=0;i<jenis;i++)
    {
        for(int j=0;j<last[i];j++)
        {
            for(int k=0;k<last[i]-j-1;k++)
            {
                if(strcmp(input[i][k], input[i][k+1]) > 0){
                    strcpy(tmp, input[i][k]);
                    strcpy(input[i][k], input[i][k+1]);
                    strcpy(input[i][k+1], tmp);
                }
            }
        }
    }
}
```

```c
void inputFile()
{
    char path[] = "shift2/drakor/";
    for (int i = 0; i < jenis; i++)
    {
        char dest[100];
        strcpy(dest, path);
        strcat(dest, kategori[i]);
        for(int j=0;j<last[i];j++)
        {
            char edit[100];
            strcpy(edit, input[i][j]);
            char filmName[50], year[5];
            int ind = 1;
            char *tok = strtok(edit, "_");
            while (tok != NULL)
            {
                if (ind % 2 == 0)
                    strcpy(filmName, tok);
                else if (ind % 2 == 1)
                    strcpy(year, tok);
                tok = strtok(NULL, ";");
                ind++;
            }
            char loc[150];
            strcpy(loc, dest);
            strcat(loc, "/data.txt");
            if (access(loc, F_OK    ) == 0)
            {
                FILE *wfile = fopen(loc, "a");
                fprintf(wfile, "\nnama : %s\nrilis : tahun %s\n", filmName, year);
                fclose(wfile);
            }
            else
            {
                FILE *wfile = fopen(loc, "w");
                fprintf(wfile, "kategori : %s\n\nnama : %s\nrilis : tahun %s\n", kategori[i], filmName, year);
                fclose(wfile);
            }
        }
    }
}
```

1. Menginisialisasi array 3D `input` bertipe char yang dimana dimensi pertama menunjukkan indeks kategori/jenis film, dimensi kedua menujukkan indeks drama korea pada suatu folder kategori, dan dimensi ketiga merupaka panjang string dari nama file. Selain itu, juga diinisialisasi array 2D `kategori` bertipe char yang dimana dimensi pertama menunjukkan indeks kategori/jenis film dan dimensi kedua merupakan panjang string dari nama file. Inisialisasi variabel `jenis` dengan nilai 0 sebagai penampung jumlah jenis film yang ada dan variabel `last[10]` yang berguna untuk menampung jumlah film pada kategori tertentu terakhir kali saat berada pada suatu folder kategori.
2. Pada saat pengerjaan soal 2b, saat membuat folder kategori, dimasukkan juga nama setiap kategori pada array 2D `kategori` dengan indeks variabel `jenis` yang bertambah 1 sehingga `jenis` hanya akan bertambah ketika membuat folder kategori baru. Contoh: kategori yang pertama kali ditemukan adalah romance, sehingga `kategori[0]` akan berisi romance, dan `jenis` akan menjadi 1. Dilakukan juga iterasi sebanyak jumlah jenis yang ada sekarang dimana di setiap iterasi akan dicek apakah kategori yang didapat sekarang sudah terdapat pada array `kategori` atau belum. Jika sudah, maka akan dibuat variabel string `tmp` yang akan menampung format string `year_filmName` untuk dimasukkan pada array 3D `input` dengan indeks dimensi pertama yaitu jenis film sekarang dan indeks dimensi kedua berupa variabel `last[i]` yaitu jumlah film terakhir pada kategori itu dengan `last[i]` ini akan bertambah 1 setiap film baru masuk pada `input`.
3. Melakukan langkah 2 terus-menerus hingga film terakhir.
4. Membuat function `sort()` yang berguna untuk mengurutkan data film di setiap kategorinya dengan membuat variabel string `tmp` untuk menampung string sementara. Iterasi dilakukan sebanyak jumlah kategori film yang ada dengan setiap iterasinya akan dilakukan `bubble sort` hingga semua film pada setiap folder kategorinya terurut berdasarkan tahun rilis (pengurutan string secara leksikografis).
5. Membuat function `inputFile()` yang berguna untuk mengirimkan semua isi array 3D `input` pada file "data.txt" sesuai folder kategorinya. Dilakukan iterasi sebanyak jumlah kategori film yang ada dimana di setiap iterasinya akan dibuat variabel string `dest` yang akan diisi path folder `drakor` kemudian ditambahkan nama kategori film pada pathnya. Lalu, akan dilakukan iterasi sebanyak `last[i]` yaitu jumlah film terakhir pada kategori tersebut dan di setiap iterasi akan dibuat variabel string `edit` dengan isinya adalah `input[i][j]` yaitu string nama film dan tahun rilis yang telah diformat menjadi `year_filmName`, kemudian  
dibuat variabel string `filmName` dan `year`, serta variabel `ind` yang diset valuenya adalah 1. Variabel `filmName` digunakan untuk menyimpan data berupa nama film dari variabel `edit` dan variabel `year` digunakan untuk menyimpan data berupa tahun rilis film dari variabel `edit`. Sedangkan untuk variabel `ind` sendiri merupakan variabel yang digunakan sebagai penanda saat pemisahan konten file (nama film dan tahun rilis film) yang akan terus bertambah 1 setiap konten file yang ditemukan. Pemisahan konten file dilakukan dengan function yang ada pada library `string.h` yaitu `strtok` dengan separator `_` hingga akhir string. Disinilah variabel-variabel tadi mulai bekerja. Saat `ind` dimodulo 2 menghasilkan nilai 0, maka konten `edit` tersebut merupakan nama film sehingga akan dimasukkan pada variabel `filmName`, sedangkan saat `ind` dimodulo 2 menghasilkan nilai 1, maka konten `edit` tersebut merupakan tahun rilis film sehingga akan dimasukkan pada variabel `year`. Selanjutnya, akan dibuat variabel string `loc` yang akan diisi variabel `dest` kemudian ditambahkan string `/data.txt` yang merupakan path lanjutan. Setelah itu akan dicek, apabila file "data.txt" pada path yang ingin diakses tersebut ditemukan, maka penambahan konten pada file menggunakan `fopen()` bertipe `a` atau `append` dengan penambahan kontennya yaitu nama film dan tahun rilis film, sedangkan apabila file "data.txt" pada path yang ingin diakses tersebut tidak ditemukan, maka penambahan konten pada file menggunakan `fopen()` bertipe `w` atau `write` dengan penambahan kontennya yaitu kategori film, nama film, dan tahun rilis film. Konten pada file akan terurut berdasarkan tahun rilis karena sudah melalui proses `sorting`.

## Dokumentasi
<img src=soal2/img1.png>
<img src=soal2/img2.png>
<img src=soal2/img3.png>
<img src=soal2/img4.png>
<img src=soal2/img5.png>
<img src=soal2/img6.png>
<img src=soal2/img7.png>

----
# SOAL 3

## Membuat Directory Darat dan Air
Diminta untuk membuat directory “/home/[USER]/modul2/” lalu diikuti dengan folder darat maupun air. Untuk itu dilakukan penulisan kode fungsi pembuatan folder sebagai berikut,
```c
void makeDir(){
	pid_t child_id;
	pid_t child_id2;
	int status;
	
	child_id = fork();
	child_id2 = fork();
	
	if (child_id < 0 || child_id2 < 0){
		exit(EXIT_FAILURE);
	}
	// membuat folder
	if (child_id == 0){
		char *argv[] = {"mkdir", "-p", "modul2/darat", NULL};
		execv("/bin/mkdir", argv);		
	}
	sleep(3);
	if (child_id2 == 0){
		char *argv[] = {"mkdir", "-p", "modul2/air", NULL};
		execv("/bin/mkdir", argv);
	}
	else{
		while((wait(&status)) > 0);
		unzipDarat();
		unzipAir();
	}
	
	return;
}
```

Dengan fungsi main dipanggil sebagai berikut,
```c
int main()
{
	makeDir();
	
	return 0;
}
```

## Extract Zip Animal.zip
Diminta untuk mengekstrak file animal.zip. Tetapi ekstrak ini harus sesuai kriteria dimana file dengan hewan darat masuk ke folder darat dan hewan air masuk ke folder air.
Unzip hewan darat:
```c
void unzipDarat(){
	pid_t child_id;
	int status;
	
	child_id = fork();
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"unzip", "animal.zip","*darat*", NULL};
		execv("/bin/unzip", argv);		
	}
	else{
		while((wait(&status)) > 0);
		moveDarat();
	}
	return;
}
```
Unzip hewan air:
```c
void unzipAir(){
	pid_t child_id;
	int status;
	
	child_id = fork();
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"unzip", "animal.zip","*air*", NULL};
		execv("/bin/unzip", argv);		
	}
	else{
		while((wait(&status)) > 0);
		moveAir();
		
	}
	return;
}
```
Dari kedua fungsi tersebut memiliki perbedaan pada perintah unzip-nya dimana untuk fungsi unzipDarat() dispesifikasikan unzipnya hanya hewan darat dengan "*darat*", bgitupun hewan air. Perbedaan selanjutnya adalah fungsi unzipDarat() memanggip fungsi moveDarat() dan untuk air memanggil fungsi moveAir(). Kedua fungsi ini untuk memindahkan unzip ke folder darat atau air.

## Extrak File ke Folder Darat maupun Air
Seperti yang disebutkan sebelumnya terdapat fungsi moveAir() dan moveDarat(). Pemindahan file ini memanfaatkan fungsi opendir dan readdir.

Kode moveDarat():
```c
void moveDarat(){
    DIR *dp;
    struct dirent *ep;
    char pathAwal[]="/home/afif/animal/";
    char pathSrc[]="/home/afif/animal/";
    int status;
    
    dp = opendir(pathAwal);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
      
          	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
          	if(fork()==0) continue;
          	strcpy(pathSrc,pathAwal);
          	strcat(pathSrc,ep->d_name);
          	
          	char *argv[] = {"mv",pathSrc, "/home/afif/modul2/darat", NULL};
          	execv("/bin/mv", argv);	
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    
    return;
}
```

Kode fungsi moveAir():
```c
void moveAir(){
    DIR *dp;
    struct dirent *ep;
    char pathAwal[]="/home/afif/animal/";
    char pathSrc[]="/home/afif/animal/";
    int status;
    
    pid_t child_id;
    
    child_id = fork();
    if (child_id < 0){
    	exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
    	dp = opendir(pathAwal);

    	if (dp != NULL)
    	{
     	while ((ep = readdir (dp))) {
      	
        	  	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
        	  	if(fork()==0) continue;
        	  	strcpy(pathSrc,pathAwal);
        	  	strcat(pathSrc,ep->d_name);
          	
        	  	char *argv[] = {"mv",pathSrc, "/home/afif/modul2/air", NULL};
        	  	execv("/bin/mv", argv);	
        }
        (void) closedir (dp);
        } else perror ("Couldn't open the directory");
    }
    else{
    	while((wait(&status)) > 0);
    	removeAnimal();
    }
    
    return;
}
```

Pada fungsi moveAir() terdapat fungsi removeAnimal(). Fungsi ini untuk menghapus folder animal yang merupakan bekas dari ekstrak file animal.zip.

Kode fungsi removeAnimal():
```c
void removeAnimal(){
	pid_t child_id;
	child_id = fork();
	int status;
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"rm","-d", "animal", NULL};
    		execv("/bin/rm", argv);		
	}
	else{
		while((wait(&status)) > 0);
		removeBird();
	}
	
	return;
}
```

## Menghapus Bird
Seperti pada fungsi sebelumnya, terdapat pemanggilan fungsi removeBird(). Fungsi ini untuk menghapus file pada darat yang mengandung kata "bird". Penghapusan ini dengan perintah rm dan memanfaatkan opendir dan readdir untuk melakukan iterasi pada tiap file di folder darat. Adapun kode fungsi removeBird() sebagai berikut,
```c
void removeBird(){
	DIR *dp;
    	struct dirent *ep;
    	char pathAwal[]="/home/afif/modul2/darat/";
    	char pathSrc[]="/home/afif/modul2/darat/";

	pid_t child_id;
	child_id = fork();
	int status;
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
    	dp = opendir(pathAwal);

    	if (dp != NULL)
    	{
     	while ((ep = readdir (dp))) {
      	
        	  	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
        	  	strcpy(pathSrc,pathAwal);
        	  	strcat(pathSrc,ep->d_name);
          		if(strstr(pathSrc,"bird")) {
          			if(fork()==0) continue;
          			removeBird2(pathSrc);
			}
        	  		
        }
        (void) closedir (dp);
        } else perror ("Couldn't open the directory");
    	}
	
}
```
Karena kode di atas belum mengeksekusi rm atau hanya melakukan iterasi pada tiap file saja, maka diperlukan fungsi untuk melakukan penghapusan file bird yang ditemukan.

Kode fungsi removeBird2():
```c
void removeBird2(char *path){
	//puts(path);
	
	pid_t child_id;
	child_id = fork();
	int status;
	
	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	else if (child_id == 0){
		char *argv[] = {"rm",path, NULL};
        	execv("/bin/rm", argv);
	}
	else{
		while((wait(&status)) > 0);
		writeTxt();
	}
        
        return;
}
```

## Membuat List
Diminta untuk membuat daftar file pada folder air dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut. Untuk itu diperlukan opendir dan readdir untuk membaca tiap file di folder air. Setiap membaca file didapatkan UID nya dan file permission-nya dan ditulis di file text dengan nama list.txt.

Kode fungsi writeTxt():
```c
void writeTxt(){
    FILE * fPtr;
    
    fPtr = fopen("/home/afif/modul2/air/list.txt", "w");

    if(fPtr == NULL)
    {
        /* File not created hence exit */
        printf("Unable to create file.\n");
        exit(EXIT_FAILURE);
    }
    
    DIR *dp;
    struct dirent *ep;
    char pathAwal[]="/home/afif/modul2/air/";
    char pathSrc[]="/home/afif/modul2/air/";
    char hasilAwal[]="";
    char hasil[]="";
    int r;
     struct stat info;
    
    dp = opendir(pathAwal);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
      
          	if(strcmp(ep->d_name,".")==0 || strcmp(ep->d_name,"..")==0) continue;
          	strcpy(pathSrc,"/home/afif/modul2/air/");
          	strcat(pathSrc,ep->d_name);
          	
          	strcpy(hasil,hasilAwal);
          	r = stat(pathSrc, &info);
    		if( r==-1 )
    		{		
      			fprintf(stderr,"File error\n");
        		exit(1);
    		}

    		struct passwd *pw = getpwuid(info.st_uid);

    		if (pw != 0) strcat(hasil,pw->pw_name);
    		strcat(hasil,"_");
    		if( info.st_mode & S_IRUSR )
        		strcat(hasil,"r");
    		if( info.st_mode & S_IWUSR )
        		strcat(hasil,"w");
    		if( info.st_mode & S_IXUSR )
        		strcat(hasil,"x");
        		
        	strcat(hasil,"_");
        	strcat(hasil,ep->d_name);
        	//puts(hasil);
        	//writeTxt(hasil);
        	fprintf(fPtr,"%s\n", hasil);
        }

      	(void) closedir (dp);
    } else perror ("Couldn't open the directory");
    
    fclose(fPtr);
}
```
