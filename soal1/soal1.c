// compile run
// gcc soal1.c -l json-c -o soal1.exe && sudo ./soal1.exe
#include<json-c/json.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <dirent.h>
#include <time.h>

#define STARTEATING if(fork()==0){
#define FORK }if(fork()==0){
#define SPOON }else{while ((wait(&status)) > 0);
#define FINISHEATING }

typedef struct {
    char name[50];
    char rarity[3];
}gachaItem;

int main(){
    pid_t pid, sid;        // Variabel untuk menyimpan PID
    int status;

    time_t startTime_t, currTime_t;
    struct tm *startTime;
    startTime = (struct tm*)malloc(sizeof(struct tm));

    //set time 30 mar 2022 4:44
    startTime->tm_year = 2022 - 1900;
    startTime->tm_mon = 3 - 1;
    startTime->tm_mday = 30;
    startTime->tm_hour = 4;
    startTime->tm_min = 44;
    startTime->tm_sec = 0;
    startTime->tm_isdst = -1;

    startTime_t = mktime(startTime);

    pid = fork(); // Menyimpan PID dari Child Process

        /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    currTime_t = time(NULL);
    while (currTime_t < startTime_t){
        sleep(1);
        currTime_t = time(NULL);
    }
    //================================SETUP (1.a)==========================================
    STARTEATING
        if (fopen("characters.zip", "r"))
        return 0;
        printf("gaada character\n");
        char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "characters.zip", NULL};
        execv("/bin/wget", argv);
    SPOON
        printf("ada character\n");
    FORK
        if(fopen("weapons.zip", "r"))return 0;
        printf("gaada weapon\n");
        char *argv[] = {"wget", "-q","https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "weapons.zip", NULL};
        execv("/bin/wget", argv);
    SPOON
        printf("ada weapon\n");
    FORK 
        if(opendir("characters")) return 0;
        char *argv[] = {"unzip","-q" ,"characters.zip", NULL};
        execv("/bin/unzip", argv);
    SPOON
    FORK
        if(!fopen("characters.zip", "r"))return 0;
        char *argv[] = {"rm", "characters.zip", NULL};
        execv("/bin/rm", argv);
    SPOON
    FORK 
        if(opendir("weapons")) return 0;
        char *argv[] = {"unzip", "-q","weapons.zip", NULL};
        execv("/bin/unzip", argv);
    SPOON
    FORK
        if(!fopen("weapons.zip", "r"))return 0;
        char *argv[] = {"rm", "weapons.zip", NULL};
        execv("/bin/rm", argv);
    SPOON
    FORK
        if(opendir("gacha_gacha"))return 0;
        char *argv[] = {"mkdir", "gacha_gacha", NULL};
        execv("/bin/mkdir", argv);
    SPOON
    FINISHEATING
    
    //count number of files in weapons and characters directory

    int wpnCnt = 0, chrCnt = 0;
    DIR  *currDir;
    struct dirent * entry;

    currDir = opendir("weapons");
    while ((entry = readdir(currDir)) != NULL)
    {
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        wpnCnt++;
    }
    closedir(currDir);

    currDir = opendir("characters");
    while ((entry = readdir(currDir)) != NULL)
    {
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        chrCnt++;
    }
    closedir(currDir);
    //test file cnting
    //printf("%d %d\n", chrCnt, wpnCnt);

    //creating memory for gacha items
    gachaItem *weapons, *characters;
    weapons = (gachaItem *)malloc(wpnCnt*sizeof(gachaItem));
    characters = (gachaItem *)malloc(chrCnt*sizeof(gachaItem));

    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;
    char *buffer;
    buffer = (char *)malloc(4096*sizeof(char));
    int cnt = 0;

    currDir = opendir("weapons");
    while ((entry = readdir(currDir)) != NULL)
    {
        char filename[50] = "weapons/";
        strcat(filename, entry->d_name);
        FILE *f = fopen(filename, "r");
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        fread(buffer, sizeof(char)*4096, 1, f);
        fclose(f);
        parsed_json = json_tokener_parse(buffer);
        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);
        //printf("%s %s\n", json_object_get_string(name), json_object_get_string(rarity));
        gachaItem *newItem = malloc(sizeof(gachaItem));
        strcpy(newItem->name, json_object_get_string(name));
        strcpy(newItem->rarity, json_object_get_string(rarity));
        weapons[cnt++] = *newItem;
    }
    closedir(currDir);

    cnt = 0;
    currDir = opendir("characters");
    while ((entry = readdir(currDir)) != NULL)
    {
        char filename[50] = "characters/";
        strcat(filename, entry->d_name);
        FILE *f = fopen(filename, "r");
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        fread(buffer, sizeof(char)*4096, 1, f);
        fclose(f);
        parsed_json = json_tokener_parse(buffer);
        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);
        //printf("%s %s\n", json_object_get_string(name), json_object_get_string(rarity));
        gachaItem *newItem = malloc(sizeof(gachaItem));
        strcpy(newItem->name, json_object_get_string(name));
        strcpy(newItem->rarity, json_object_get_string(rarity));
        characters[cnt++] = *newItem;
    }
    closedir(currDir);

    //test parsing
    // printf("%s %s\n", weapons[0].name, weapons[0].rarity);
    // printf("%s %s\n", characters[0].name, characters[0].rarity);

    chdir("gacha_gacha");

    int gachaTries = 0;
    int primogems = 79000;
    int primoCost = 160;

    srand(time(NULL));
    int randomNum;
    char folderName[100];

    //============================LOOPING :D========================
    while(1){
        if(primogems <= primoCost)
            break;
        time_t now = time(NULL);
        struct tm *tm_struct = localtime(&now);
        char filename[100];
        char filePath[100];
        char triesStr[10];
        sprintf(triesStr, "%d", gachaTries);

        strftime(filename, 20, "%H:%M:%S", tm_struct);
        strcat(filename,"_gacha_");
        strcat(filename, triesStr);
        strcat(filename, ".txt");

        if(gachaTries % 90 == 0){
            strcpy(folderName, "total_gacha_");
            strcat(folderName, triesStr);
            STARTEATING
                if(opendir(folderName))return 0;
                char *argv[] = {"mkdir", folderName, NULL};
                execv("/bin/mkdir", argv);
            SPOON
            FINISHEATING
        }
        strcpy(filePath, folderName);
        strcat(filePath, "/");
        strcat(filePath, filename);

        
        STARTEATING
            char *argv[] = {"touch", filePath, NULL};
            execv("/bin/touch", argv);
        SPOON
            FILE *f = fopen(filePath, "w");
            char fileContent[400]="";
            for (int i = 0; i < 10; i++)
            {
                primogems -= primoCost;
                gachaItem gachaHaha;
                if (++gachaTries % 2)
                {                                   //ganjil = character
                    randomNum = rand() % chrCnt;
                    gachaHaha = characters[randomNum];
                }
                else
                {                                   //genap = weapon
                    randomNum = rand() % wpnCnt;
                    gachaHaha = weapons[randomNum];
                }
                char primoStr[6];
                sprintf(triesStr, "%d", gachaTries);
                sprintf(primoStr, "%d", primogems);
                strcat(fileContent, triesStr);
                strcat(fileContent, "_");
                strcat(fileContent, (gachaTries%2 ? "characters":"weapons"));
                strcat(fileContent, "_");
                strcat(fileContent, gachaHaha.rarity);
                strcat(fileContent, "_");
                strcat(fileContent, gachaHaha.name);
                strcat(fileContent, "_");
                strcat(fileContent, primoStr);
                strcat(fileContent, "\n");
            
                if (primogems <= primoCost)
                    break;
            }
            strcat(fileContent, "\0");
            printf("%s", fileContent);
            fwrite(fileContent, sizeof(char), strlen(fileContent), f);
            fclose(f);
            FINISHEATING

            sleep(1);
    }
    //nunggu sampe 7:44, 30 Maret
    currTime_t = time(NULL);
    int delaySeconds = 3600 * 3;
    while (currTime_t < startTime_t + delaySeconds)
    {
        sleep(1);
        currTime_t = time(NULL);
    }

    chdir("..");
    STARTEATING
        char *argv[] = {"zip", "--password", "satuduatiga", "-r","not_safe_for_wibu.zip", "gacha_gacha", NULL};
        execv("/bin/zip", argv);
    SPOON
    FORK
        char *argv[] = {"rm", "-r", "gacha_gacha" ,NULL};
        execv("/bin/rm", argv);
    SPOON
    FORK
        char *argv[] = {"rm", "-r", "characters" ,NULL};
        execv("/bin/rm", argv);
    SPOON
    FORK
        char *argv[] = {"rm", "-r", "weapons" ,NULL};
        execv("/bin/rm", argv);
    FINISHEATING

    printf("done\n");
    return 0;
}